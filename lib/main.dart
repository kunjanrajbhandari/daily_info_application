import 'package:flutter/material.dart';

import 'covid/covid_main.dart';
import 'exchange/exchange_main.dart';
import 'horoscope/horoscope.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MyApp(),
  ));
}
class MyApp extends StatefulWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff35375D),
      body: Column(
        children: [
          Stack(
            children: [
              Transform.rotate(
                origin: Offset(30, -60),
                angle: 2.4,
                child: Container(
                  margin: EdgeInsets.only(
                    left: 75,
                    top: 40,
                  ),
                  height: 400,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(80),
                    gradient: LinearGradient(
                      begin: Alignment.bottomLeft,
                      colors: [Color(0xffFD8BAB), Color(0xFFBD3434)],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 70),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      'Daily Information App',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 26,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Your daily source of inspiration \nand knowledge',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 50),
                      child: Column(
                        
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap:(){ Navigator.push(context, MaterialPageRoute(builder: (context)=> CovidPage()));},
                                child: Container(
                                  height: 150.0,
                                  width: 175.0,
                                  decoration: BoxDecoration(
                                    color: Color(0xff050505).withOpacity(0.52),
                                    borderRadius: BorderRadius.all(Radius.circular(20.0))
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Image.asset(
                                        'assets/image/coronavirus.png',
                                        height: 73.0,
                                        width: 73.0,
                                      ),
                                      Text(
                                        "Active Case: 123",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white
                                        )
                                      ),
                                      Text(
                                        "Recovered: 123",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> RateMain()));
                                },
                                child: Container(
                                  height: 150.0,
                                  width: 175.0,
                                  decoration: BoxDecoration(
                                    color: Color(0xff050505).withOpacity(0.52),
                                    borderRadius: BorderRadius.all(Radius.circular(20.0))
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Image.asset(
                                        'assets/image/exchange.png',
                                        height: 80.0,
                                        width: 80.0,
                                      ),
                                      Text(
                                        "US: 123",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white
                                        )
                                      ),
                                      Text(
                                        "Dubai: 33",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white
                                        ),
                                      ),
                                      Text(
                                        "India: 1.60",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 22.0)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> HoroscopeMain()));
                                },
                                child: Container(
                                  height: 150.0,
                                  width: 175.0,
                                  decoration: BoxDecoration(
                                    color: Color(0xff050505).withOpacity(0.52),
                                    borderRadius: BorderRadius.all(Radius.circular(20.0))
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Image.asset(
                                        'assets/image/horoscope.png',
                                        height: 73.0,
                                        width: 73.0,
                                      ),
                                      Text(
                                        "Horoscope",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white
                                        )
                                      ),
                                      
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                height: 150.0,
                                width: 175.0,
                                decoration: BoxDecoration(
                                  color: Color(0xff050505).withOpacity(0.52),
                                  borderRadius: BorderRadius.all(Radius.circular(20.0))
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Image.asset(
                                      'assets/image/cloudy.png',
                                      height: 80.0,
                                      width: 80.0,
                                    ),
                                    Text(
                                      "Weather Forecaste",
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white
                                      )
                                    ),
                                    
                                  ],
                                ),
                              ),
                            ],
                          ),
                         Padding(padding: EdgeInsets.only(top: 22.0)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 150.0,
                                width: 175.0,
                                decoration: BoxDecoration(
                                  color: Color(0xff050505).withOpacity(0.52),
                                  borderRadius: BorderRadius.all(Radius.circular(20.0))
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    //Padding(padding: EdgeInsets.only(top: 5.0)),
                                    Image.asset(
                                      'assets/image/mars.png',
                                      height: 73.0,
                                      width: 73.0,
                                    ),
                                    //Padding(padding: EdgeInsets.only(top: 33.0)),
                                    Text(
                                      "Nasa Rover",
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white
                                      )
                                    ),
                                    
                                  ],
                                ),
                              ),
                              Container(
                                height: 150.0,
                                width: 175.0,
                                decoration: BoxDecoration(
                                  color: Color(0xff050505).withOpacity(0.52),
                                  borderRadius: BorderRadius.all(Radius.circular(20.0))
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Image.asset(
                                      'assets/image/laughing.png',
                                      height: 80.0,
                                      width: 80.0,
                                    ),
                                    Text(
                                      "Meme",
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white
                                      )
                                    ),
                                    
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    
    );
  }
}