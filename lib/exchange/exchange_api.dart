import 'dart:convert';

import 'package:dailyinformationapp/exchange/rate.dart';
import 'package:http/http.dart' as http;

class ExchangeApi{
  List<RateModel> getExchange =[];

  var url = Uri.parse('https://www.nrb.org.np/api/forex/v1/rates?from=2021-07-27&to=2021-07-27&per_page=100&page=1');
  Future<List<RateModel>> rateExchange()async{
    var buyRate;
    var sellRate;
    var unit;
    var countryName;

    var response = await http.get(url);
    dynamic result = json.decode(response.body);
    if(response.statusCode == 200){
        
      for(int i=0; i<result.length; i++){
        getExchange.add(RateModel(
          name: result["data"]["payload"][0]["rates"][i]["currency"]["name"],
          unit: result["data"]["payload"][0]["rates"][i]["currency"]["unit"],
          buy:  result["data"]["payload"][0]["rates"][i]["buy"],
          sell: result["data"]["payload"][0]["rates"][i]["sell"],
          iso: result["data"]["payload"][0]["rates"][i]["currency"]["iso3"]

        ));
      }
      return getExchange;
    }
    else{
      throw Exception();
    }
  }

}