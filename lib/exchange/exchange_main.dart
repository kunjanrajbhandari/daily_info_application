import 'dart:convert';

import 'package:dailyinformationapp/exchange/rate.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class RateMain extends StatefulWidget {
  const RateMain({ Key? key }) : super(key: key);

  @override
  _RateMainState createState() => _RateMainState();
}

class _RateMainState extends State<RateMain> {
  List<RateModel> getExchange =[];
  var url = Uri.parse('https://www.nrb.org.np/api/forex/v1/rates?from=2021-07-27&to=2021-07-27&per_page=100&page=1');
  Future<List<RateModel>> rateExchange()async{


    var response = await http.get(url);
    dynamic result = json.decode(response.body);
    if(response.statusCode == 200){
        
      for(int i=0; i<result.length; i++){
        getExchange.add(RateModel(
          name: result["data"]["payload"][0]["rates"][i]["currency"]["name"],
          unit: result["data"]["payload"][0]["rates"][i]["currency"]["unit"],
          buy:  result["data"]["payload"][0]["rates"][i]["buy"],
          sell: result["data"]["payload"][0]["rates"][i]["sell"],
          iso: result["data"]["payload"][0]["rates"][i]["currency"]["iso3"]
        ));
      }
      return getExchange;
    }
    else{
      throw Exception();
    }
  }

  @override
  void initState() {
    rateExchange();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff35375D),
      body:  Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(top: 22.0)),
            IconButton(
              onPressed: (){
                Navigator.pop(context);
              }, 
              icon: Icon(
                Icons.arrow_back,
                color: Color(0xffEFEFEF),
              ),
            ),
   
            Padding(
              padding: EdgeInsets.only(left: 11.0,bottom: 8.0),
              child: Column(
                children: [
                  Center(
                    child: Text(
                      "Currency Exchange Rate",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: Colors.white
                      ),
                    ),
                  ),
                    
                ],
              ),
            ),
             Container(
                height: MediaQuery.of(context).size.height-105,
                width: MediaQuery.of(context).size.width  ,
                
                child: FutureBuilder(
                future: rateExchange(),
                builder: ( context,  snapshot) {
                 if(snapshot.hasData){
                   return ListView.builder(
                     scrollDirection: Axis.vertical,
                     itemCount: getExchange.length,
                     itemBuilder: ( context, position) {
                     return  Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: Container(
                            width: 200.0,
                            height: 200.0,
                           decoration: BoxDecoration(
                             borderRadius: BorderRadius.circular(18.0),
                             
                             color: Colors.black.withOpacity(0.5) ,
                           ),
                           child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                             children: [
                               Text(
                                 getExchange[position].name,
                                 style: TextStyle(
                                   fontSize: 20.0,
                                   color: Colors.white
                                ),
                              ),
                             
                                Padding(
                                  padding: EdgeInsets.only(left: 22.0),
                                  child: Table(
                                    border: TableBorder.symmetric(),
                                    children: [
                                      TableRow(
                                        children: [
                                          
                                          Text("Currency", style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),),
                                          Text("Quantity",style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),),
                                          Text("Buy Rate",style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),),
                                          Text("Sell Rate",style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),)
                                        ],
                                        
                                      ),
                                     
                                      TableRow(
                                        children: [
                                          
                                
                                           Text(
                                            getExchange[position].iso,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.white.withOpacity(0.8),
                                            ),
                                          ),
                                
                                          Text(
                                            getExchange[position].unit.toString(),
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.white.withOpacity(0.8),
                                            ),
                                          ),
                                           Text(
                                            getExchange[position].buy,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.white.withOpacity(0.8),
                                            ),
                                          ),
                                          Text(
                                            getExchange[position].sell,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              color: Colors.white.withOpacity(0.8),
                                            ),
                                          ),
                                          
                                          
                                        ]
                                      )
                                    ],
                                  ),
                                )
                             ],
                          ),
                         ),
                       
                     );
                    },
                   );
                 } 
                  else{
                    return Text("Loading...");
                  }
                },
              ),

              ),
            
            
          ],
        ),
    );
  }
}