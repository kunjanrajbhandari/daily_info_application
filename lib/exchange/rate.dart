class RateModel{
  String name;
  int unit;
  String buy;
  String sell;
  String iso;

  RateModel({required this.name, required this.unit, required this.buy, required this.sell, required this.iso});
}