class Horoscope{
  String dateRange;
  String currentDate;
  String description;
  String compatibility;
  String mood;
  String color;
  String  luckyNumber;
  String luckyTime;

  Horoscope({required this.dateRange, required this.currentDate, required this.description, required this.compatibility, required this.mood, required this.color, required this.luckyNumber, required this.luckyTime, });
}