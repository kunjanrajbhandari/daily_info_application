import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HoroscopeMain extends StatefulWidget {
  const HoroscopeMain({ Key? key }) : super(key: key);

  @override
  _HoroscopeMainState createState() => _HoroscopeMainState();
}

class _HoroscopeMainState extends State<HoroscopeMain> {
  List horoscopeValue = ['aries', 'taurus', 'gemini', 'cancer', 'leo', 'virgo', 'libra', 'scorpio', 'sagittarius', 'capricorn', 'aquarius', 'pisces'];
  var value='aries';
  var result;

  
  Future horoscopeData()async{
  var url = Uri.parse('https://aztro.sameerkumar.website/?sign=$value&day=today');
    var response = await http.post(url);
    result = json.decode(response.body);
    if(response.statusCode == 200){
     
    return result;
    }
    else{
      throw Exception();
    }
  }
  @override
  void initState() {
    horoscopeData();
    super.initState();
  }
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff35375D),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(padding: EdgeInsets.only(top: 22.0)),
          IconButton(
            onPressed: (){
              Navigator.pop(context);
            }, 
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xffEFEFEF),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 444.0,
              width: 444.0,
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.7),
                borderRadius: BorderRadius.circular(18),
              ),
              child: FutureBuilder(
                future: horoscopeData(),
                builder: ( context,  snapshot) {
                  if(snapshot.hasData){
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,

                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left:255.0),
                          child: Text(
                            result["date_range"],
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 17.0  
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: 5.0)),
                        Center(
                          child: Text(
                            "Description",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0  ,
                                fontWeight: FontWeight.bold
                              ),
                          ),
                        ),
                        Text(
                          result["description"],
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17.0  
                            ),
                        ),
                       Padding(
                         padding: const EdgeInsets.only(top:11.0),
                         child: Text(
                           "Compatibilty: ",style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0  ,
                                  fontWeight: FontWeight.bold
                                ),
                      ),
                       ),
                            Text(
                              result["compatibility"],
                             style: TextStyle(
                              color: Colors.white,
                              fontSize: 17.0  
                            
                          ),
                        ),

                        Text(result["current_date"]),
                        Text(result["mood"]),
                        Text(result["color"]),
                        Text(result["lucky_number"]),
                        Text(result["lucky_time"]),
                      
                      ],
                    );
                    
                  }
                  else{
                    return Text("Loading....");
                  }
                },
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 88.0)),
          Container(
            height: 111.0,
            width: double.infinity,
            child: ListView.builder(
              itemCount: horoscopeValue.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: ( context,  position) {
                
              return Padding(
                padding: EdgeInsets.all(8),
                child: GestureDetector(
                  onTap: (){
                    setState(() {
                      
                    value = horoscopeValue[position];
                    horoscopeData();
                    });
                  },
                  child: Container(
                    
                    width: 111.0,
                    decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.6),
                      borderRadius: BorderRadius.circular(19.0)
                    ),
                    child: Center(
                      child: Text(
                        horoscopeValue[position],
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Colors.white
                        ),
                      )
                    ),
                  ),
                ),
              );
             },
            ),
          ),
          
        ],
      ),
      
    );
  }
}