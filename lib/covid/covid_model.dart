
class CovidModel{
  final dynamic totalConfirmed;
  final String country;
  final dynamic totalDeaths;
  final dynamic totalRecovered;

  CovidModel({required this.totalConfirmed, required this.country, required this.totalDeaths, required this.totalRecovered});
  
}
class CountryFlag{
  dynamic flag;
  dynamic countryName;
  
  CountryFlag({required this.flag, required this.countryName});
}