import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

import 'covid_model.dart';


class CovidPage extends StatefulWidget {
  const CovidPage({ Key? key }) : super(key: key);

  @override
  _CovidPageState createState() => _CovidPageState();
}

class _CovidPageState extends State<CovidPage> {
  List<CountryFlag> getFlag =[];
  var flagURL = Uri.parse('https://restcountries.eu/rest/v2/all');
  Future<List<CountryFlag>> countryFlag()async{
    var flagResponse = await http.get(flagURL);
    dynamic flagResult = json.decode(flagResponse.body);
    if(flagResponse.statusCode == 200){
     for(int i=0; i<flagResult.length; i++){  
        getFlag.add(CountryFlag(
          flag: flagResult[i]['flag'],
          countryName: flagResult[i]['name'],
        ));
      }
      return getFlag;
    }
    else{
      throw Exception();
    }
  }
  
  @override
  void initState() {    
    countryFlag();
    
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff35375D),
      body:  Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(top: 22.0)),
            IconButton(
              onPressed: (){
                Navigator.pop(context);
              }, 
              icon: Icon(
                Icons.arrow_back,
                color: Color(0xffEFEFEF),
              ),
            ),
            Container(
              
              padding: EdgeInsets.only(top: 11.0,left: 13.0),
              child: Stack(
                children: [
                  //Padding(padding: EdgeInsets.only(top:55.0)),
                  Container(
                    height: 165,
                    width: 385,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Color(0xffEFEFEF),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 33.0),
                    child: Image.asset(
                      'assets/image/—Pngtree—cartoon hand drawn protective clothing_5342035.png',
                      height: 285.0,
                      width: 285.0,
                    )
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 11.0),
              child: Column(
                children: [
                  Text(
                    "Covid in Nepal",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: Colors.white
                    ),
                  ),
                    
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 15),
              child: Container(
                height: 222,
                width: MediaQuery.of(context).size.width  ,
                
                child: FutureBuilder(
                  future: countryFlag(),
                  builder: ( context,  snapshot) {
                    if(snapshot.hasData){ 
                      print("length of data: ${getFlag.length}");
                      return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: getFlag.length,
                  itemBuilder: (context, position){
                    return Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Container(
                        width: 100,
                        height: 221,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: 
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(getFlag[position].countryName),
                            Expanded(
                              child: SvgPicture.network(
                              getFlag[position].flag,
                            )),
                          ],
                        ),
                    
                      ),
                    );
                  },
                );
                    }
                    else{
                      return CircularProgressIndicator();
                    }
                  },
                ),

              ),
            )
            
          ],
        ),
      
        
      
    );
  }
}

