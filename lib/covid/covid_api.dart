
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'covid_model.dart';
class CovidAPI{
  List<CovidModel> getDetails =[];
  

  var url = Uri.parse('https://api.covid19api.com/summary');
  

    Future<List<CovidModel>> covidData()async{
      var response = await http.get(url);
      dynamic result = json.decode(response.body);
      if(response.statusCode == 200){
        for (int i=0; i< result.length; i++){ 


          getDetails.add(CovidModel(
            country: result['Countries'][i]['Country'],
            totalConfirmed: result['Countries'][i]['TotalConfirmed'],
            totalDeaths: result['Countries'][i]['TotalDeaths'],
            totalRecovered: result['Countries'][i]['TotalRecovered'],
          ));
          
        }
        return getDetails;
      }
      else{
        throw Exception();
      }
      

  }
  

}